# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

FROM ubuntu:noble
LABEL maintainer="Mark Jonas <toertel@gmail.com>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && \
    apt-get -y install \
        # corresponds to SWUpdate's ci/setup.sh 93974c5
        build-essential \
        automake \
        cmake \
        curl \
        libzmq3-dev \
        liblua5.2-dev \
        libluajit-5.1-dev \
        libconfig-dev \
        libarchive-dev \
        libbtrfsutil-dev \
        libjson-c-dev \
        libyaml-dev \
        zlib1g-dev \
        git \
        uuid \
        uuid-dev \
        liblzo2-dev \
        libsystemd-dev \
        libsystemd0 \
        check \
        librsync2 \
        librsync-dev \
        libext2fs-dev \
        liburiparser-dev \
        doxygen \
        graphviz \
        autoconf-archive \
        linux-headers-generic \
        libmbedtls-dev \
        libcmocka-dev \
        libfdisk-dev \
        libwebsockets-dev \
        libgpiod-dev \
        libcurl4-openssl-dev \
        libpci-dev \
        gawk \
        cpio \
        meson \
        ninja-build \
        libzstd-dev \
        wget \
        python3 \
        libebgenv-dev \
        libmtd-dev \
        libubi-dev \
        libubootenv-dev \
        libzck-dev \
        # bear is used to create compile_commands.json for Sourcetrail
        # https://www.sourcetrail.com/
        bear \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY ./prepare.sh /
RUN /prepare.sh

COPY ./entrypoint.sh /
COPY ./runtest.sh /

WORKDIR /mnt

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/runtest.sh"]
