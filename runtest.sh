#!/bin/bash

# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

## @brief Compile and test a given defconfig
## @param 1 name of an existing defconfig, e.g. raspi_defconfig
## @param 2 optional make prefix, e.g. bear
## @return Value which shows whether build and testing succeeded
## @retval 0 if building and running the test succeeded
## @retval 1 if loading the given defconfig failed
## @retval 2 if compiling the given defconfig failed
## @retval 3 if the test suite failed
runtest() {
    echo
    echo "Building and testing config $1 ..."

    # load defconfig
    if ! make "$1"; then
        return 1
    fi

    # compile (with optional make prefix)
    if ! $2 make -j "$(nproc)"; then
        return 2
    fi

    # run tests
    if ! make tests; then
        return 3
    fi

    return 0
}

# Build and test all existing defconfigs
if [ "$#" -eq 0 ]; then
    echo
    echo "Building and testing all configs ..."
    if [ ! -d "configs" ]; then
        echo "Directory 'configs' does not exist." >&2
        exit 1
    fi

    typeset -i i=0
    for confpath in configs/*; do
        config=$(basename "$confpath")
        configs[i]="$config"
        results[i]=-1
        i=$((i + 1))
    done

    typeset -i i=0
    for config in "${configs[@]}"; do
        runtest "$config"
        results[i]=$?
        i=$((i + 1))
    done

    echo
    echo "Test result overview:"
    typeset -i i=0 pass=0
    for config in "${configs[@]}"; do
        case ${results[i]} in
        0) result="pass" pass=$((pass + 1)) ;;
        1) result="FAIL (config)" ;;
        2) result="FAIL (build)" ;;
        3) result="FAIL (test)" ;;
        *) result="FAIL (skipped)" ;;
        esac
        echo "$config: $result"
        i=$((i + 1))
    done

    echo
    echo "$pass of $i test suites succeeded."

    echo
    if ((pass == i)); then
        echo "PASS"
        exit 0
    else
        echo "FAIL"
        exit 3
    fi
fi

# Build and test a single defconfig
if [ "$#" -eq 1 ]; then
    runtest "$1"
    exit $?
fi

# Build and test a single defconfig
# and also generate a compile_commands.json file, e.g. for Sourcetrail
if [ "$#" -eq 2 ]; then
    if [ "$2" != "bear" ]; then
        echo "The only supported option is 'bear'." >&2
        exit 1
    fi
    if [ -f compile_commands.json ]; then
        rm compile_commands.json
    fi
    make mrproper
    runtest "$1" "bear --"
    exit $?
fi

echo "Usage: $0 [CONFIG] [OPTION]" >&2
exit 1
