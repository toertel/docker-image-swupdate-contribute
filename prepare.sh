#!/bin/sh

# SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

# Exit immediately if a command exits with a non-zero status
set -e

# Mitigation for git's message
#     detected dubious ownership in repository at '/mnt'
git config --global --add safe.directory /mnt

# Corresponds to SWUpdate's ci/setup.sh 93974c5
PC_FILE_DIR=$(pkg-config --variable=pcfiledir lua5.2)
ln -sf "$PC_FILE_DIR/lua5.2.pc" "$PC_FILE_DIR/lua.pc"

# Symlink runtest for easy calling
ln -s /runtest.sh /usr/bin/runtest
