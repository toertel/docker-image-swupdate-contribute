<!--
SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/docker-image-swupdate-contribute/badges/master/pipeline.svg)](https://gitlab.com/toertel/docker-image-swupdate-contribute/pipelines)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/toertel/docker-image-swupdate-contribute)](https://api.reuse.software/info/gitlab.com/toertel/docker-image-swupdate-contribute)

# Docker container for SWUpdate contribution

You can either build your own image or pull a pre-built image from Docker Hub.

## Build image

```
docker build -t "toertel/swupdate-contribute" .
```

## Pull pre-Built image

```
docker pull "toertel/swupdate-contribute"
```

# Run container

When running the container you can either build and test all existing
defconfigs or just a single one.

Besides building and testing the container can also be used to generate a
`compile_commands.json` file, e.g. to dig into the source code using
[*Sourcetrail*](https://github.com/CoatiSoftware/Sourcetrail).

## Test all defconfigs

Assuming you are in the root directory of a *SWUpdate* repository you can
start a build and test of all defconfigs using the following command.

```
docker run --rm -v "$PWD":/mnt "toertel/swupdate-contribute"
```

At the end of the run an overview which test suites passed and which failed
will be printed.

## Test single defconfig

Give the command `runtest` and the name of a defconfig as the parameter when
starting the container to build and test only a single configuration.

```
docker run --rm -v "$PWD":/mnt "toertel/swupdate-contribute" runtest raspi_defconfig
```

## Test and generate `compile_commands.json`

This is equivalent to testing a single defconfig but will also generate a
`compile_commands.json` file. Note the last parameter *bear* in the command
below.

```
docker run --rm -v "$PWD":/mnt "toertel/swupdate-contribute" runtest raspi_defconfig bear
```

> The container expects the *SWUpdate* source code in */mnt*. Thus, the
> generated `compile_commands.json` will refer to */mnt* as the source
> directory. Before starting [*Sourcetrail*](https://www.sourcetrail.com/) you
> will either have to mount the *SWUpdate* source directory to */mnt*, too, or
> you need to replace all occurrences of */mnt* in `compile_commands.json` with
> your actual *SWUpdate* source path.

## Do whatever you want

You can use the container for what you think it shall do by passing your own
command and paramters. For example, you can start a Bash shell.

```
docker run -it --rm -v "$PWD":/mnt "toertel/swupdate-contribute" /bin/bash
```

Or you can use it to compile SWUpdate.

```
docker run --rm -v "$PWD":/mnt "toertel/swupdate-contribute" make raspi_defconfig swupdate
```
